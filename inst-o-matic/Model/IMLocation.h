//
//  SDLocation.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import Foundation;
#import "IMBaseModel.h"

// Sample JSON structure
//
// "location" : {
//    "longitude" : 106.814773337,
//    "id" : 288726390,
//    "latitude" : -6.258739792,
//    "name" : "Kampung Kemang"
// }

// JSON fields
#define kJSON_LOCATION_LATITUDE @"latitude"
#define kJSON_LOCATION_LONGITUDE @"longitude"
#define kJSON_LOCATION_NAME @"name"

@interface IMLocation : NSObject<IMBaseModel>

@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, strong) NSString *name;

@end
