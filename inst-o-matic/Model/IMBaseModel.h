//
//  SDBaseModel.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IMBaseModel <NSObject>

- (instancetype) initWithDictionary:(NSDictionary *)dict;

@end
