//
//  SDLocation.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMLocation.h"
#import "IMConstants.h"

@implementation IMLocation

-(instancetype)initWithDictionary:(NSDictionary *)dict {
    if(self = [super init]) {
        self.latitude = [dict[kJSON_LOCATION_LATITUDE] floatValue];
        self.longitude = [dict[kJSON_LOCATION_LATITUDE] floatValue];
        self.name = dict[kJSON_LOCATION_NAME];
    }
    return self;
}

@end
