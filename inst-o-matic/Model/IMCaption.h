//
//  SDCaption.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import Foundation;
#import "IMBaseModel.h"

// Sample JSON structure
//
// "caption" : {
//    "from" : {
//        "id" : "226490805",
//        "profile_picture" : "http://photos-e.ak.instagram.com/hphotos-ak-xfa1/10665911_699989433425540_303988655_a.jpg",
//        "username" : "kevin_pourtois",
//        "full_name" : "Kev"
//    },
//    "id" : "815993272862022306",
//    "text" : "#Spaclassic#spa#francorchamps#bmw#bmwlowered#2002ti",
//    "created_time" : "1411493999"
// }

#define kJSON_CAPTION_FROM @"from"
#define kJSON_CAPTION_USERNAME @"username"
#define kJSON_CAPTION_TEXT @"text"
#define kJSON_CAPTION_PROFILE_PICTURE @"profile_picture"

@interface IMCaption : NSObject<IMBaseModel>

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *text;

@end
