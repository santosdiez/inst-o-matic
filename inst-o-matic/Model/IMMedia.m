//
//  SDPhoto.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMMedia.h"

@implementation IMMedia

-(instancetype) initWithDictionary:(NSDictionary *)dict {
    if(self = [super init]) {
        self.mediaId = dict[kJSON_MEDIA_ID];
        self.createdTime = [dict[kJSON_MEDIA_CREATED_TIME] integerValue];
        self.thumbnailUrl = [NSURL URLWithString:dict[kJSON_MEDIA_IMAGES][kJSON_MEDIA_THUMBNAIL][kJSON_MEDIA_URL]];
        self.standardUrl = [NSURL URLWithString:dict[kJSON_MEDIA_IMAGES][kJSON_MEDIA_STANDARD_RESOLUTION][kJSON_MEDIA_URL]];
        self.profilePicture = [NSURL URLWithString:dict[kJSON_MEDIA_USER][kJSON_MEDIA_USER_PROFILE]];
        
        if(dict[kJSON_MEDIA_CAPTION] != [NSNull null]) {
            self.caption = [[IMCaption alloc] initWithDictionary:dict[kJSON_MEDIA_CAPTION]];
        }
        
        if(dict[kJSON_MEDIA_LOCATION] != [NSNull null]) {
            self.location = [[IMLocation alloc] initWithDictionary:dict[kJSON_MEDIA_LOCATION]];
        }
    }
    return self;
}

-(BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[IMMedia class]]) {
        return NO;
    }
    
    IMMedia *other = (IMMedia *)object;
    return [other.mediaId isEqualToString:self.mediaId];
}

-(NSUInteger)hash {
    return [self.mediaId hash];
}

@end
