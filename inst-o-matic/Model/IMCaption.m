//
//  SDCaption.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMCaption.h"

@implementation IMCaption

-(instancetype)initWithDictionary:(NSDictionary *)dict {
    if(self = [super init]) {
        self.username = dict[kJSON_CAPTION_FROM][kJSON_CAPTION_USERNAME];
        self.text = dict[kJSON_CAPTION_TEXT];
    }
    
    return self;
}

@end
