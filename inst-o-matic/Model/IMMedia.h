//
//  SDPhoto.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import Foundation;
#import "IMCaption.h"
#import "IMLocation.h"
#import "IMBaseModel.h"

// Sample JSON structure
//
// {
//    "id" : "815993375069037093_1322706015",
//    "comments" : {
//        ...
//    },
//    "created_time" : "1411494012",
//    "caption" : {
//        ...
//    },
//    "users_in_photo" : [],
//    "link" : "http://instagram.com/p/tS_aa8D-Il/",
//    "likes" : {
//        "data" : [],
//        "count" : 0
//    },
//    "tags" : [
//              ...
//              ],
//    "type" : "video",
//    "location" : null,
//    "filter" : "Normal",
//    "images" : {
//        "thumbnail" : {
//            "height" : 150,
//            "width" : 150,
//            "url" : "http://scontent-a.cdninstagram.com/hphotos-xpa1/t51.2885-15/10518175_1479082899036378_505084844_s.jpg"
//        },
//        "low_resolution" : {
//            "height" : 306,
//            "width" : 306,
//            "url" : "http://scontent-a.cdninstagram.com/hphotos-xpa1/t51.2885-15/10518175_1479082899036378_505084844_a.jpg"
//        },
//        "standard_resolution" : {
//            "height" : 640,
//            "width" : 640,
//            "url" : "http://scontent-a.cdninstagram.com/hphotos-xpa1/t51.2885-15/10518175_1479082899036378_505084844_n.jpg"
//        }
//    },
//    "user" : {
//        "website" : "",
//        "id" : "1322706015",
//        "profile_picture" : "http://photos-c.ak.instagram.com/hphotos-ak-xfa1/10661249_356471364507810_733044631_a.jpg",
//        "bio" : "",
//        "username" : "cars_go_fast_",
//        "full_name" : "CarsGoFast - Car Spotters "
//    },
//    "videos" : {
//        ...
//    },
//    "attribution" : null
// }

// JSON fields
#define kJSON_MEDIA_ID @"id"
#define kJSON_MEDIA_CREATED_TIME @"created_time"
#define kJSON_MEDIA_CAPTION @"caption"
#define kJSON_MEDIA_TYPE @"type"
#define kJSON_MEDIA_IMAGES @"images"
#define kJSON_MEDIA_THUMBNAIL @"low_resolution"
#define kJSON_MEDIA_STANDARD_RESOLUTION @"standard_resolution"
#define kJSON_MEDIA_URL @"url"
#define kJSON_MEDIA_LOCATION @"location"
#define kJSON_MEDIA_USER @"user"
#define kJSON_MEDIA_USER_PROFILE @"profile_picture"

@interface IMMedia : NSObject<IMBaseModel>

@property (nonatomic, strong) NSString *mediaId;
@property (nonatomic, assign) NSUInteger createdTime;
@property (nonatomic, strong) NSURL *profilePicture;
@property (nonatomic, strong) NSURL *thumbnailUrl;
@property (nonatomic, strong) NSURL *standardUrl;
@property (nonatomic, strong) IMCaption *caption;
@property (nonatomic, strong) IMLocation *location;

@end
