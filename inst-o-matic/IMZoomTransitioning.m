//
//  IMZoomAnimationController.m
//  inst-o-matic
//
//  Created by Borja Santos-Diez on 25/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMZoomTransitioning.h"

@implementation IMZoomTransitioning

-(instancetype)init {
    return [self initWithDuration:0.5 frame:CGRectZero];
}

-(instancetype)initWithDuration:(NSTimeInterval)duration frame:(CGRect)frame {
    if(self = [super init]) {
        _duration = duration;
        _frame = frame;
    }
    return self;
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

// Zoom in transition
-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *container = [transitionContext containerView];
    
    // Do some math...
    CGFloat sx = self.frame.size.width/toViewController.view.frame.size.width;
    
    CGPoint center = CGPointMake(self.frame.origin.x + self.frame.size.width/2, self.frame.origin.y + self.frame.size.height/2);
    
    if (!self.reverse) {
        // Use the same value for both dimensions to scale proportionally
        toViewController.view.transform = CGAffineTransformMakeScale(sx, sx);
        toViewController.view.center = center;
        [container addSubview:toViewController.view];
    }
    
    [UIView animateKeyframesWithDuration:self.duration delay:0 options:0 animations:^{
        
        if (self.reverse) {
            fromViewController.view.transform = CGAffineTransformMakeScale(sx, sx);
            fromViewController.view.center = center;
            
            toViewController.view.alpha = 1;
        }
        else {
            toViewController.view.transform = CGAffineTransformIdentity;
            toViewController.view.center = fromViewController.view.center;
            
            fromViewController.view.alpha = 0.2;
        }
        
        
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:finished];
    }];
}

@end
