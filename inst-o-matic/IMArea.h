//
//  IMArea.h
//  inst-o-matic
//
//  Created by Borja on 28/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface IMArea : NSObject

- (instancetype)initWithWidth:(CGFloat)width;
- (CGPoint)addRect:(CGRect)rect;

@end
