//
//  SDConstants.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#ifndef inst_o_matic_SDConstants_h
#define inst_o_matic_SDConstants_h

#define kINSTAGRAM_CLIENT_ID @"4839cfd448ad4551b6dc3fd116b53550"
#define kINSTAGRAM_API_URL @"https://api.instagram.com/v1/"
#define kINSTAGRAM_API_TAGS [NSString stringWithFormat:@"%@/tags/%%@/media/recent?count=100&client_id=%@", kINSTAGRAM_API_URL, kINSTAGRAM_CLIENT_ID]

#define kPHOTO_CELL_IDENTIFIER @"PhotoCell"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#endif
