//
//  ViewController.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMPhotoStreamViewController.h"
#import "IMConstants.h"
#import "IMMedia.h"
#import "IMCollectionViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>
#import "IMPhotoViewController.h"

// JSON fields
#define kJSON_DATA @"data"

@interface IMPhotoStreamViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UIViewControllerTransitioningDelegate>
@property (nonatomic, strong) NSMutableSet *media;
@property (nonatomic, strong) NSArray *mediaArray;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, assign) NSIndexPath *selectedIndexPath;
@property (nonatomic, assign) NSUInteger columns;
@property (nonatomic, assign) CGFloat itemWidth;

- (void)addMedia:(NSSet *)objects;
- (void)loadData;
- (void)refresh:(id)sender;

@end

@implementation IMPhotoStreamViewController

#pragma mark Lifecycle

- (instancetype)init {
    
    UICollectionViewFlowLayout *flow = [UICollectionViewFlowLayout new];
    flow.minimumInteritemSpacing = 0;
    flow.minimumLineSpacing = 0;
    
    if(self = [super initWithCollectionViewLayout:flow]) {
        // Set delegates
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        // Register cell
        [self.collectionView registerClass:[IMCollectionViewCell class] forCellWithReuseIdentifier:kPHOTO_CELL_IDENTIFIER];
        
        // Extra setup
        self.collectionView.bounces = YES;
        self.collectionView.alwaysBounceVertical = YES;
        self.collectionView.alwaysBounceHorizontal = NO;
        
        self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background"]];
        self.collectionView.backgroundView.alpha = 0.5;
        self.collectionView.backgroundView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set refresh control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    NSAttributedString *refreshTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pull to refresh", @"Refresh control title") attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    refreshControl.attributedTitle = refreshTitle;
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    
    self.zoomTransitioning = [IMZoomTransitioning new];
    
    self.media = [NSMutableSet new];
    self.mediaArray = [NSArray new];
    
    // Read plist with tags
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tags" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    self.tags = [NSArray arrayWithArray:dict[@"tags"]];
    
    // Extra values
    self.columns = floorf(self.view.bounds.size.width/100);
    self.itemWidth = self.view.bounds.size.width/self.columns;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refresh:(id)sender {
    __weak UIRefreshControl *control = ((UIRefreshControl *)sender);
    [self loadData];
    [control endRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Loading...", @"Loading message in the HUD");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadData];
}

- (void)loadData {

    NSString *plainTag, *url;
    NSURLRequest *request;
    AFHTTPRequestOperation *operation;
    
    [self.media removeAllObjects];
    self.mediaArray = [NSArray new];
    
    [self.collectionView reloadData];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    
    // Really ugly, but due to Instagram's API limitations (only one tag per request)
    // we need to do it this way and 'merge' the results
    for(NSString *tag in self.tags) {
        
        // Remove any # from the tags
        plainTag = [tag stringByReplacingOccurrencesOfString:@"#" withString:@""];
        url = [NSString stringWithFormat:kINSTAGRAM_API_TAGS, plainTag];
        
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer new];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSMutableSet *set = [NSMutableSet new];
            
            for(NSDictionary *dict in responseObject[kJSON_DATA]) {
                [set addObject:[[IMMedia alloc] initWithDictionary:dict]];
            }
            
            // Add media including the current tag
            [self addMedia:set];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", [error description]);
        }];
        
        [queue addOperation:operation];
    }
    
    // Once all the operations are finished, we can end refreshing
    [queue waitUntilAllOperationsAreFinished];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)addMedia:(NSSet *)objects {
    
    NSMutableSet *newSet = [NSMutableSet setWithSet:objects];
    [newSet minusSet:self.media];
    [self.media unionSet:objects];
 
    NSSortDescriptor *createdTimeDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdTime" ascending:NO];
    
    self.mediaArray = [[self.media allObjects] sortedArrayUsingDescriptors:@[createdTimeDescriptor]];
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    for(IMMedia *media in newSet) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:[self.mediaArray indexOfObject:media] inSection:0]];
    }
    
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
    
}

#pragma mark Rotation events

// iOS 7
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    self.columns = floorf(self.view.bounds.size.height/100);
    self.itemWidth = self.view.bounds.size.height/self.columns;
    [self.collectionView.collectionViewLayout invalidateLayout];
}

// iOS 8
-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    self.columns = floorf(size.width/100);
    self.itemWidth = size.width/self.columns;
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark UICollectionView delegate and datasource methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.itemWidth, self.itemWidth);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // iOS 8 specific issue, due to the removal of the status bar in landscape mode
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        CGFloat top = (collectionView.frame.size.width < collectionView.frame.size.height) ? 20 : 0;
        return UIEdgeInsetsMake(top, 0, 0, 0);
    }
    return UIEdgeInsetsMake(20, 0, 0, 0);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IMCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPHOTO_CELL_IDENTIFIER forIndexPath:indexPath];
    IMMedia *media = self.mediaArray[indexPath.row];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:media.thumbnailUrl];
    
    // Prevent retain cycles
    __weak IMCollectionViewCell *weakCell = cell;
    
    [cell.imageView setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"Placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        // Add some nice cross disolve transition from the placeholder to the actual image
        [UIView transitionWithView:weakCell.imageView
            duration:0.2
            options:UIViewAnimationOptionTransitionCrossDissolve
            animations:^{
                weakCell.imageView.image = image;
            }
            completion:nil];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error loading image: %@", [error description]);
    }];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.mediaArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    
    IMCollectionViewCell *selectedCell = (IMCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    IMMedia *element = self.mediaArray[indexPath.row];
    
    IMPhotoViewController *controller = [[IMPhotoViewController alloc] initWithMedia:element placeholder:selectedCell.imageView.image];
    
    controller.transitioningDelegate = self;
    controller.delegate = self;
    controller.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark UIViewControllerTransitioningDelegate methods

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:self.selectedIndexPath];
    CGRect frame = [self.collectionView convertRect:attributes.frame toView:self.view];
    
    IMZoomTransitioning *transitioning = [[IMZoomTransitioning alloc] initWithDuration:0.3 frame:frame];
    return transitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:self.selectedIndexPath];
    CGRect frame = [self.collectionView convertRect:attributes.frame toView:self.view];
    
    IMZoomTransitioning *transitioning = [[IMZoomTransitioning alloc] initWithDuration:0.3 frame:frame];
    transitioning.reverse = YES;
    return transitioning;
}

#pragma mark IMPhotoDetailViewControllerDelegate methods

-(void)dismissDetailViewController {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

@end
