//
//  IMPhotoDetailViewController.m
//  inst-o-matic
//
//  Created by Borja on 27/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import MapKit;
#import "IMPhotoDetailViewController.h"
#import <Masonry/Masonry.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>

@interface IMPhotoDetailViewController ()

@property (nonatomic, strong) UIView *detailsView;
@property (nonatomic, strong) MKMapView *mapView;

- (void)dismiss:(id)sender;

@end

@implementation IMPhotoDetailViewController

-(instancetype)initWithMedia:(IMMedia *)media {
    if(self = [super init]) {
        _media = media;
        self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.view.backgroundColor = [UIColor blackColor];
    }
    return self;
}

-(void)viewDidLoad {

    NSString *dismissLabel = NSLocalizedString(@"Dismiss", @"Dismiss detailed view");
    
    UIButton *dismiss = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [dismiss setTitle:dismissLabel forState:UIControlStateNormal];
    [dismiss addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:dismiss];
    
    // Profile image with border and rounded corners
    UIImageView *profilePicture = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    profilePicture.contentMode = UIViewContentModeScaleToFill;
    profilePicture.layer.borderColor = [[UIColor whiteColor] CGColor];
    profilePicture.layer.borderWidth = 2.0;
    profilePicture.layer.cornerRadius = 5.0;
    profilePicture.layer.masksToBounds = YES;
    profilePicture.image = [UIImage imageNamed:@"ProfileImage"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.media.profilePicture];
    
    __weak UIImageView *weakProfilePicture = profilePicture;
    
    // Load profile picture with fade in
    [profilePicture setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"ProfileImage"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [UIView transitionWithView:weakProfilePicture
            duration:0.3
            options:UIViewAnimationOptionTransitionCrossDissolve
            animations:^{
                weakProfilePicture.image = image;
            }
            completion:nil];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error loading profile image: %@", [error description]);
    }];
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    username.text = [NSString stringWithFormat:@"@%@", self.media.caption.username];
    username.textColor = [UIColor whiteColor];
    
    UILabel *caption = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    caption.numberOfLines = 6;
    caption.preferredMaxLayoutWidth = self.view.frame.size.width;
    caption.textColor = [UIColor whiteColor];
    caption.text = self.media.caption.text;
    
    // Map view. Rounded corners
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    self.mapView.layer.cornerRadius = 5.0;
    self.mapView.layer.masksToBounds = YES;
    
    [self.view addSubview:profilePicture];
    [self.view addSubview:username];
    [self.view addSubview:caption];
    [self.view addSubview:self.mapView];
    
    // Masonry (autolayout)
    [dismiss mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right).with.offset(-10);
    }];
    
    [profilePicture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(30);
        make.left.equalTo(self.view.mas_left).with.offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    [username mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(profilePicture.mas_right).with.offset(20);
        make.centerY.equalTo(profilePicture);
    }];
    
    [caption mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(profilePicture.mas_bottom).with.offset(20);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.centerX.equalTo(self.view);
    }];
    
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.top.equalTo(caption.mas_bottom).with.offset(20);
        make.bottom.equalTo(self.view).with.offset(-40);
    }];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.media.location) {
        MKCoordinateRegion region;
        region.center.latitude = self.media.location.latitude;
        region.center.longitude = self.media.location.longitude;
        region.span.latitudeDelta = 0.5;
        region.span.longitudeDelta = 0.5;
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = region.center;
        annotation.title = self.media.location.name;
        
        [self.mapView addAnnotation:annotation];
        [self.mapView setRegion:region animated:YES];
        [self.mapView selectAnnotation:annotation animated:YES];
    } else {
        UIView *overlay = [[UIView alloc] initWithFrame:self.mapView.bounds];
        overlay.backgroundColor = [UIColor blackColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, overlay.bounds.size.width, 20)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = NSLocalizedString(@"Location not available", @"Location not available");
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:20];
        [overlay addSubview:label];
        overlay.alpha = 0;
        
        [self.mapView addSubview:overlay];
        
        // Autolayout
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(overlay);
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            overlay.alpha = 0.5;
        }];
    }
    
}

// Target for the dismiss button
- (void)dismiss:(id)sender {
    [self.delegate dismissDetailViewController];
}

@end
