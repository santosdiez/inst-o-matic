//
//  PhotoDetailViewController.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMPhotoDetailViewController.h"
#import "IMMedia.h"

@protocol IMPhotoViewControllerDelegate <NSObject>

- (void) dismissPhotoViewController;

@end

@interface IMPhotoViewController : UIViewController<UIScrollViewDelegate, IMPhotoDetailViewControllerDelegate>

@property (nonatomic, assign) id<IMPhotoDetailViewControllerDelegate> delegate;
@property (nonatomic, strong) UIImageView *imageView;

- (instancetype) initWithMedia:(IMMedia *)media placeholder:(UIImage *)placeholder;

@end
