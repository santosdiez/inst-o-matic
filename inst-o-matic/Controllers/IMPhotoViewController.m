//
//  PhotoDetailViewController.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMPhotoViewController.h"
#import <Masonry/Masonry.h>
#import "IMPhotoDetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface IMPhotoViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *dismissButton;
@property (nonatomic, strong) UIButton *detailsButton;
@property (nonatomic, strong) IMMedia *media;
@property (nonatomic, strong) UIImage *placeholder;

- (void) dismiss;
- (void) showDetails;
- (void) tapTwice:(UIGestureRecognizer *)recognizer;

@end

@implementation IMPhotoViewController

-(instancetype)initWithMedia:(IMMedia *)media placeholder:(UIImage *)placeholder {
    if(self = [super init]) {
        _media = media;
        _placeholder = placeholder;
        self.view.backgroundColor = [UIColor blackColor];
    }
    return self;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale=1;
    self.scrollView.maximumZoomScale=3.0;
    
    UITapGestureRecognizer *tapTwice = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTwice:)];
    tapTwice.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:tapTwice];
    
    [self.view addSubview:self.scrollView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.scrollView.frame];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.scrollView addSubview:self.imageView];
    
    self.dismissButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.imageView.frame.size.width/2, 30)];
    [self.dismissButton setTitle:NSLocalizedString(@"Dismiss", @"Dismiss button title") forState:UIControlStateNormal];
    [self.dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    self.detailsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.imageView.frame.size.width/2, 30)];
    [self.detailsButton setTitle:NSLocalizedString(@"Details", @"Details button title") forState:UIControlStateNormal];
    [self.detailsButton addTarget:self action:@selector(showDetails) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.dismissButton];
    [self.view addSubview:self.detailsButton];
    
    // Masonry
    
    [self.dismissButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(10);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self.detailsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).with.offset(-10);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Make the black background appear smoothly
    self.view.backgroundColor = [UIColor clearColor];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.backgroundColor = [UIColor blackColor];
    }];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.media.standardUrl];
    
    __weak UIImageView *weakImageView = self.imageView;
    
    // Use the thumbnail as placeholder while the full-res image is loaded
    [self.imageView setImageWithURLRequest:request placeholderImage:self.placeholder success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        // Add some nice cross disolve transition from the placeholder to the actual image
        [UIView transitionWithView:weakImageView
            duration:0.3
            options:UIViewAnimationOptionTransitionCrossDissolve
            animations:^{
                weakImageView.image = image;
            }
            completion:nil];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error loading big image: %@", [error description]);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.dismissButton.alpha = 0;
        self.detailsButton.alpha = 0;
        self.scrollView.zoomScale = self.scrollView.minimumZoomScale;
        self.view.backgroundColor = [UIColor clearColor];
    }];
    
    [self.delegate dismissDetailViewController];
}

-(void)showDetails {
    
    // Reset zoom before the transition
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.zoomScale = self.scrollView.minimumZoomScale;
    }];

    IMPhotoDetailViewController *controller = [[IMPhotoDetailViewController alloc] initWithMedia:self.media];
    controller.delegate = self;
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)tapTwice:(UIGestureRecognizer *)recognizer {
    
    if(self.scrollView.zoomScale > self.scrollView.minimumZoomScale) {
        [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
    } else {
        CGPoint point = [recognizer locationInView:recognizer.view];
        [self.scrollView zoomToRect:CGRectMake(point.x, point.y, 0, 0) animated:YES];
    }
}

#pragma mark IMPhotoDetailViewControllerDelegate methods

-(void)dismissDetailViewController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
