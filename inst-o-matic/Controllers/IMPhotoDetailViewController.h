//
//  IMPhotoDetailViewController.h
//  inst-o-matic
//
//  Created by Borja on 27/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMMedia.h"

@protocol IMPhotoDetailViewControllerDelegate <NSObject>

-(void) dismissDetailViewController;

@end

@interface IMPhotoDetailViewController : UIViewController

@property (nonatomic, assign) id<IMPhotoDetailViewControllerDelegate> delegate;
@property (nonatomic, strong) IMMedia *media;

- (instancetype)initWithMedia:(IMMedia *)media;

@end
