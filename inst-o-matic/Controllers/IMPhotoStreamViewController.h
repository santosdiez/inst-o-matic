//
//  ViewController.h
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMZoomTransitioning.h"
#import "IMPhotoViewController.h"

@interface IMPhotoStreamViewController : UICollectionViewController<IMPhotoDetailViewControllerDelegate>

@property (nonatomic, strong) IMZoomTransitioning *zoomTransitioning;

@end

