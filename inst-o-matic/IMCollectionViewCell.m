//
//  SDCollectionViewCell.m
//  inst-o-matic
//
//  Created by Borja on 23/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMCollectionViewCell.h"

@implementation IMCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

@end
