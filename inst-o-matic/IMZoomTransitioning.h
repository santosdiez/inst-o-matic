//
//  IMZoomAnimationController.h
//  inst-o-matic
//
//  Created by Borja Santos-Diez on 25/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface IMZoomTransitioning : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) BOOL reverse;

- (instancetype)initWithDuration:(NSTimeInterval)duration frame:(CGRect)frame;

@end
