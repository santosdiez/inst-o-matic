//
//  IMCollectionViewCustomLayout.h
//  inst-o-matic
//
//  Created by Borja on 26/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IMCollectionViewCustomLayout : UICollectionViewLayout

-(CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
