//
//  IMArea.m
//  inst-o-matic
//
//  Created by Borja on 28/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMArea.h"

@interface IMArea ()

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, strong) NSMutableSet *boundary;

- (NSMutableSet *)grahamScan;
- (CGFloat)ccw:(CGPoint)p1 P2:(CGPoint)p2 P3:(CGPoint)p3;
- (CGPoint)firstAvailablePoint;
- (NSInteger)compare:(CGPoint)p1 withPoint:(CGPoint)p2 andReference:(CGPoint)p0;
- (CGFloat)dist:(CGPoint)p1 toPoint:(CGPoint)p2;

@end

@implementation IMArea

-(instancetype)initWithWidth:(CGFloat)width {
    if(self = [super init]) {
        _boundary = [NSMutableSet set];
        _width = width;
    }
    return self;
}

-(CGPoint)addRect:(CGRect)rect {
    
    // Get vertices
    NSArray *vertices = @[
        [NSValue valueWithCGPoint:CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect))],
        [NSValue valueWithCGPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))],
        [NSValue valueWithCGPoint:CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect))],
        [NSValue valueWithCGPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))]
    ];
    
    [self.boundary addObjectsFromArray:vertices];
    self.boundary = [self grahamScan];
    
    return [self firstAvailablePoint];
}

-(NSMutableSet *)grahamScan {
    
    // Implement Graham scan
    // http://en.wikipedia.org/wiki/Graham_scan
    
    NSMutableArray *points = [NSMutableArray arrayWithArray:[self.boundary allObjects]];
    CGPoint point;
    
    // Find lowest Y
    CGPoint p0 = {MAXFLOAT,MAXFLOAT};
    
    for(NSValue *val in points) {
        point = [val CGPointValue];
    
        if(point.y < p0.y && point.x < p0.x) {
            p0 = point;
        }
    }
    
    // Sort points
    [points sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CGPoint point1 = [obj1 CGPointValue];
        CGPoint point2 = [obj2 CGPointValue];
        
        NSInteger result = [self compare:point1 withPoint:point2 andReference:p0];
        
        if(result == 0) {
            return NSOrderedSame;
        }
        
        return (result < 0) ? NSOrderedAscending : NSOrderedDescending;
    }];
    
    // Create stack
    NSMutableArray *s = [NSMutableArray array];
    [s insertObject:points[0] atIndex:0];
    [s insertObject:points[1] atIndex:0];
    [s insertObject:points[2] atIndex:0];
    
    for(int i = 3; i < points.count; i++) {
        while (s.count > 3 && [self ccw:[s[1] CGPointValue] P2:[s[0] CGPointValue] P3:[points[i] CGPointValue]] > 0) {
            // Pop item
            [s removeObjectAtIndex:0];
        }
        [s insertObject:points[i] atIndex:0];
    }
    
    return [NSMutableSet setWithArray:s];
}

-(NSInteger)compare:(CGPoint)p1 withPoint:(CGPoint)p2 andReference:(CGPoint)p0 {
    // Find orientation
    CGFloat angleA = (p0.x - p1.x) / (p1.y - p0.y);
    CGFloat angleB = (p0.x - p2.x) / (p2.y - p0.y);
    
    CGFloat result = -angleA + angleB;
    
    if (angleA == angleB) {
        double distA = [self dist:p0 toPoint:p1];
        double distB = [self dist:p0 toPoint:p2];
        
        result = distA - distB;
    }
    
    if(result == 0) {
        return NSOrderedSame;
    }
    
    return result < 0 ? NSOrderedAscending : NSOrderedDescending;
 
}

-(CGFloat)dist:(CGPoint)p1 toPoint:(CGPoint)p2 {
    return (p2.x-p1.x)/sqrt((p2.x-p1.x)*(p2.x-p1.x) + (p2.y-p1.y)*(p2.y-p1.y));
}

-(CGFloat)ccw:(CGPoint)p1 P2:(CGPoint)p2 P3:(CGPoint)p3 {
    return (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
}

-(CGPoint)firstAvailablePoint {
    
    // Sort points by Y coord
    NSMutableArray *points = [[[self.boundary allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CGPoint p1 = [obj1 CGPointValue];
        CGPoint p2 = [obj2 CGPointValue];
        
        NSComparisonResult result;
        
        if(p1.y < p2.y) {
            result = NSOrderedAscending;
        } else if (p1.y > p2.y) {
            result = NSOrderedDescending;
        } else {
            // Y coord are the same
            result = (p1.x < p2.x) ? NSOrderedAscending : NSOrderedDescending;
        }
        return result;
    }] mutableCopy];
    
    CGPoint prev, current, next;
    
    for(int i = 1; i < points.count - 1; i++) {
        prev = [points[i-1] CGPointValue];
        current = [points[i] CGPointValue];
        next = [points[i+1] CGPointValue];
        
        if(current.y == prev.y && current.y == next.y) {
            [points removeObjectAtIndex:i];
        }
    }
    
    CGPoint tmp;
    
    for(NSValue *val in points) {
        tmp = [val CGPointValue];
        
        if(tmp.x + tmp.y != 0 && tmp.x < self.width) {
            return tmp;
        }
    }
    
    return CGPointZero;
}

@end
