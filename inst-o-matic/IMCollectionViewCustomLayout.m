//
//  IMCollectionViewCustomLayout.m
//  inst-o-matic
//
//  Created by Borja on 26/09/2014.
//  Copyright (c) 2014 santosdiez.es. All rights reserved.
//

#import "IMCollectionViewCustomLayout.h"
#import "IMArea.h"
#import "IMConstants.h"

@interface IMCollectionViewCustomLayout ()

@property (nonatomic, assign) CGPoint firstOpenSpace;
@property (nonatomic, assign) CGPoint furthestDown;
@property (nonatomic, assign) CGRect latestFrame;
@property (nonatomic, assign) NSUInteger columns;
@property (nonatomic, strong) NSDictionary *layoutInfo;
@property (nonatomic, strong) NSMutableDictionary *widths;
@property (nonatomic, strong) IMArea *area;

- (CGRect)frameForPhotoAtIndexPath:(NSIndexPath *)indexPath;

@end

@implementation IMCollectionViewCustomLayout

- (void)prepareLayout {
    self.firstOpenSpace = CGPointZero;
    self.furthestDown = CGPointZero;
    self.latestFrame = CGRectZero;
    self.columns = floorf(self.collectionView.bounds.size.width/100);
    self.widths = [NSMutableDictionary new];
    self.area = [[IMArea alloc] initWithWidth:self.collectionView.bounds.size.width];
    
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView numberOfSections];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < itemCount; item++) {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            
            UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            itemAttributes.frame = [self frameForPhotoAtIndexPath:indexPath];
            
            cellLayoutInfo[indexPath] = itemAttributes;
        }
    }
    
    // Set it as a dictionary, so that we could potentially have different types of cells
    self.layoutInfo = @{kPHOTO_CELL_IDENTIFIER : cellLayoutInfo};
}

-(CGRect)frameForPhotoAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect final;
    CGFloat width = self.collectionView.bounds.size.width;
    CGFloat itemWidth = width/self.columns;
    
    if(arc4random()%4 == 0) {
        itemWidth *= 2;
    }
    itemWidth = MIN(itemWidth, width - self.firstOpenSpace.x);
    
//    NSLog(@"----------");
//    NSLog(@"Width: %f", width);
//    NSLog(@"Item: %ld", (long)indexPath.row);
//    NSLog(@"First open space: (%f,%f)", self.firstOpenSpace.x, self.firstOpenSpace.y);
//    NSLog(@"Last frame: (%f,%f,%f,%f)", self.latestFrame.origin.x, self.latestFrame.origin.y, self.latestFrame.size.width, self.latestFrame.size.height);
//    NSLog(@"Item width: %f", itemWidth);
    
    final = CGRectMake(self.firstOpenSpace.x, self.firstOpenSpace.y, itemWidth, itemWidth);
    
    self.firstOpenSpace = [self.area addRect:final];
    
    self.widths[@(indexPath.row)] = [NSValue valueWithCGSize:final.size];
    
//    NSLog(@"Final frame: (%f, %f, %f, %f)", final.origin.x, final.origin.y, final.size.width, final.size.height);
    
//    CGFloat positionX = self.firstOpenSpace.x + itemWidth;
//    CGFloat positionY = self.firstOpenSpace.y + itemWidth;
    
//    NSLog(@"PositionX: %f", positionX);
//    NSLog(@"PositionY: %f", positionY);
    
//    // Subtract a small amount for safety
//    if(positionY > self.furthestDown.y - 5) {
//        // Start new row
//        self.furthestDown = CGPointMake(self.firstOpenSpace.x, positionY);
//    }
//    
//    // Subtract a small amount for safety
//    if(positionX > width - 5) {
//        // We are at the right border of the screen
//        
//        if(positionY < self.furthestDown.y) {
//            self.firstOpenSpace = CGPointMake(self.firstOpenSpace.x, self.firstOpenSpace.y + itemWidth);
//        } else {
//            self.firstOpenSpace = CGPointMake(self.furthestDown.x, self.firstOpenSpace.y + itemWidth);
//        }
//    } else {
//        self.firstOpenSpace = CGPointMake(positionX, self.firstOpenSpace.y);
//    }
    
    self.latestFrame = final;
    
    return final;
    
}

- (CGSize)collectionViewContentSize
{
    CGSize contentSize = self.collectionView.frame.size;
    contentSize.height = self.latestFrame.origin.y + self.latestFrame.size.height;
    return contentSize;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier,
                                                         NSDictionary *elementsInfo,
                                                         BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                          UICollectionViewLayoutAttributes *attributes,
                                                          BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                [allAttributes addObject:attributes];
            }
        }];
    }];
    
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[kPHOTO_CELL_IDENTIFIER][indexPath];
}

-(CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.widths[@(indexPath.row)] CGSizeValue];
}

@end